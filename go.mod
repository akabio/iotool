module gitlab.com/akabio/iotool

go 1.17

require (
	github.com/akabio/expect v0.10.0
	gitlab.com/akabio/expect v0.9.9
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
