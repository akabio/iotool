package iotool

import (
	"path/filepath"
	"strings"
)

func FileWithExtension(file, ext string) string {
	return strings.TrimSuffix(file, filepath.Ext(file)) + "." + ext
}
