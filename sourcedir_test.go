package iotool_test

import (
	"testing"

	"github.com/akabio/expect"
	"gitlab.com/akabio/iotool"
)

func TestMustGetSourceDir(t *testing.T) {
	expect.Value(t, "source dir", iotool.MustGetSourceDir()).ToHaveSuffix("/iotool")
}
