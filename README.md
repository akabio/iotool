# iotool

IO Tools/utils

## Write file only if changed

WriteFileIfChanged will write the file only if it does not exist already with the exact same content. It will delegate to ioutil.WriteFile if necssary. If the old file could not be read due to any error it will try to write the new content and return this error if it fails. If the file did not change it will prevent changing of the modification date nor trigger any listeners like inotify.

    err := WriteFileIfChanged("test.txt", data, 0) error

## Get folder of the calling go source file

MustGetSourceDir returns the folder of where the calling go source file resides. If it can not be evaluated due to missing information in executable it will panic. Intended usage is for things like build tools or generators and not programs or libraries.

    folder := MustGetSourceDir()


## Lookup Env variables

LookupEnv finds the env var value or returns the provided default. Will also print the found value to stdout with masked passwords as a json object.

Passwords will be masked for:
  - Env vars with suffix _PASSWORD, _KEY or _SECRET
  - URLs

    val := LookupEnv("URL", "http://default")
    # http://foo:paxxxxx@localhost