package iotool

import (
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"strings"
)

type status struct {
	Var     string `json:"var"`
	Value   string `json:"value"`
	Default bool   `json:"default"`
}

var maskSuffixes = []string{"PASSWORD", "KEY", "SECRET"}

// LookupEnv finds the env var value or returns the provided default.
// Will also print the found value to stdout with masked passwords as a
// json object.
//
// Passwords will be masked for:
//   - Env vars with suffix _PASSWORD, _KEY or _SECRET
//   - URLs
func LookupEnv(name string, def string) string {
	value, found := os.LookupEnv(name)

	if !found {
		value = def
	}

	st := status{
		Var:     name,
		Value:   mask(name, value),
		Default: !found,
	}

	js, err := json.Marshal(&st)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(js))

	return value
}

func mask(name, value string) string {
	for _, suf := range maskSuffixes {
		if strings.HasSuffix(name, suf) {
			return value[:2] + "xxxxx"
		}
	}

	u, err := url.Parse(value)
	if err == nil {
		pw, hasPw := u.User.Password()
		if hasPw {
			u.User = url.UserPassword(u.User.Username(), pw[:2]+"xxxxx")
			return u.String()
		}
	}

	return value
}
