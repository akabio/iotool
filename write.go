package iotool

import (
	"bytes"
	"io/ioutil"
	"os"
)

// WriteFileIfChanged will write the file only if it does not exist already with the exact same content. It will
// delegate to ioutil.WriteFile if necssary. If the old file could not be read due to any error it will try to write the
// new content and return this error if it fails.
// If the file did not change it will prevent changing of the modification date nor trigger any listeners like inotify.
func WriteFileIfChanged(filename string, data []byte, perm os.FileMode) error {
	oldData, err := ioutil.ReadFile(filename)

	if err == nil && bytes.Equal(oldData, data) {
		// the content of the file is not changed, leave it as it is
		return nil
	}

	return ioutil.WriteFile(filename, data, perm)
}
