package iotool

import (
	"testing"

	"github.com/akabio/expect"
)

func TestFileWithExtension(t *testing.T) {
	type fwx struct {
		file     string
		ext      string
		expected string
	}

	testCases := []fwx{{
		file:     "a/b/test.obj",
		ext:      "png",
		expected: "a/b/test.png",
	}, {
		file:     "a/b/黄.山",
		ext:      "png",
		expected: "a/b/黄.png",
	}, {
		file:     "zebra",
		ext:      "png",
		expected: "zebra.png",
	}, {
		file:     "zebra.stripe.obj",
		ext:      "png",
		expected: "zebra.stripe.png",
	}}

	for _, tc := range testCases {
		expect.Value(t, "with extension", FileWithExtension(tc.file, tc.ext)).ToBe(tc.expected)
	}
}
