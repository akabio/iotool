package iotool

import (
	"testing"

	"gitlab.com/akabio/expect"
)

func TestMask(t *testing.T) {
	tc := func(name, value, expected string) {
		t.Helper()

		expect.Value(t, "masked value", mask(name, value)).ToBe(expected)
	}

	tc("FOO", "test", "test")
	tc("FOO_PASSWORD_XX", "test", "test")
	tc("FOO_PASSWORD", "test", "texxxxx")
	tc("FOO_KEY", "test", "texxxxx")
	tc("FOO_SECRET", "test", "texxxxx")

	tc("FOO", "http://testuser:secretpassword@foo.com:1234/bla", "http://testuser:sexxxxx@foo.com:1234/bla")
	tc("FOO", "http://testuser@foo.com:1234/bla", "http://testuser@foo.com:1234/bla")
}
