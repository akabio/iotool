package iotool

import (
	"fmt"
	"time"
)

type TimeRecorder struct {
	started time.Time
}

func Time() *TimeRecorder {
	return &TimeRecorder{
		started: time.Now(),
	}
}

func (t *TimeRecorder) Println(message string) {
	fmt.Println(message, time.Since(t.started))
	t.started = time.Now()
}
