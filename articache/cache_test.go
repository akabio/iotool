package articache

import (
	"os"
	"testing"
	"time"

	"github.com/akabio/expect"
)

func TestOpenFile(t *testing.T) {
	_, err := Open("test/file.txt")
	expect.Value(t, "error", err).Message().ToBe("mkdir test/file.txt: not a directory")
}

func TestOpenSubFolder(t *testing.T) {
	err := os.RemoveAll("test/folder")
	if err != nil {
		t.Fatal(err)
	}
	_, err = Open("test/folder")
	if err != nil {
		t.Fatal(err)
	}
}

func TestReadMissingFile(t *testing.T) {
	c := newTmpCache(t)
	defer c.close()

	val, exists, err := c.c.Get([]byte("labdfcr"))
	expect.Error(t, err).ToBe(nil)
	expect.Value(t, "exists", exists).ToBe(false)
	expect.Value(t, "value", val).ToCount(0)
}

func TestReadExistingFile(t *testing.T) {
	c := newTmpCache(t)
	defer c.close()

	err := c.c.Put([]byte("labdfcr"), []byte("foo"))
	expect.Error(t, err).ToBe(nil)

	val, exists, err := c.c.Get([]byte("labdfcr"))
	expect.Error(t, err).ToBe(nil)
	expect.Value(t, "exists", exists).ToBe(true)
	expect.Value(t, "value", val).ToBe([]byte("foo"))
}

func TestExpireAfterOneHour(t *testing.T) {
	c := newTmpCache(t)
	defer c.close()

	err := c.c.Put([]byte("labdfcr"), []byte("foo"))
	expect.Error(t, err).ToBe(nil)

	c.delta = time.Hour

	val, exists, err := c.c.Get([]byte("labdfcr"))
	expect.Error(t, err).ToBe(nil)
	expect.Value(t, "exists", exists).ToBe(false)
	expect.Value(t, "value", val).ToCount(0)
}

type tmpCache struct {
	t     *testing.T
	dir   string
	c     Cache
	delta time.Duration
}

func newTmpCache(t *testing.T) *tmpCache {
	dir, err := os.MkdirTemp("", "goiotooltest")
	if err != nil {
		t.Fatal(err)
	}
	c, err := Open(dir)
	if err != nil {
		t.Fatal(err)
	}
	tc := &tmpCache{
		t:     t,
		dir:   dir,
		c:     c,
		delta: time.Second * 0,
	}
	c.(*cache).now = func() time.Time {
		return time.Now().Add(tc.delta)
	}
	return tc
}

func (t *tmpCache) close() {
	err := os.RemoveAll(t.dir)
	if err != nil {
		t.t.Fatal(err)
	}
}
