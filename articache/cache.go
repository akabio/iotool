package articache

// A simple filesystem based cache.
// It has no locking mechanism and the expire time is one hour since write.
// The intended use case is to have a cache for cli utils that get called frequently.
// There is no cleanup mechanism.

import (
	"crypto/md5"
	"encoding/base64"
	"os"
	"path"
	"time"
)

// Cache defines the Get and Put methods for the interface
type Cache interface {
	Get([]byte) ([]byte, bool, error)
	Put([]byte, []byte) error
}

type cache struct {
	dir string
	now func() time.Time
}

// Open uses given directory as the cache location
func Open(dir string) (Cache, error) {
	err := os.MkdirAll(dir, 0o777)
	if err != nil {
		return nil, err
	}

	c := &cache{
		dir: dir,
		now: func() time.Time {
			return time.Now()
		},
	}

	return c, nil
}

func (c *cache) Get(key []byte) ([]byte, bool, error) {
	filePath := path.Join(c.dir, c.hash(key))
	fi, err := os.Stat(filePath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, false, nil
		}
		return nil, false, err
	}
	if fi.ModTime().Before(c.now().Add(-time.Hour)) {
		// to old
		return nil, false, nil
	}

	d, err := os.ReadFile(filePath)
	if err != nil {
		return nil, false, err
	}
	return d, true, nil
}

func (c *cache) Put(key []byte, value []byte) error {
	filePath := path.Join(c.dir, c.hash(key))
	err := os.MkdirAll(path.Dir(filePath), 0o777)
	if err != nil {
		return err
	}

	return os.WriteFile(filePath, value, 0o666)
}

// hash creates a path from the hash of the given bytes
func (c *cache) hash(data []byte) string {
	h := md5.New()
	h.Write(data) // nolint
	res := h.Sum(nil)

	hash := base64.URLEncoding.EncodeToString(res)
	return path.Join(hash[:2], hash[2:14])
}
