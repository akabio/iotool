package iotool

import (
	"path/filepath"
	"runtime"
)

// MustGetSourceDir returns the folder of where the calling go source file resides.
// If it can not be evaluated due to missing information in executable it will panic.
// Intended usage is for things like build tools or generators and not programs
// or libraries.
func MustGetSourceDir() string {
	_, filename, _, ok := runtime.Caller(1)
	if !ok {
		panic("could not evaluate the directory of the go source file")
	}
	return filepath.Dir(filename)
}
